import Computer from "@components/Computer.tsx";

export default function IndexPage() {
  return (
    <>
      <div class="w-32 h-32 border rounded-full border-solid border-4">
      </div>
      <div class="w-96">
        <Computer />
      </div>
    </>
  );
}
