import { useSignal } from "@preact/signals";
import { useEffect } from "preact/hooks";
import Button from "@components/Button.tsx";

export default function JokePage() {
  const joke = useSignal("");

  const fetchData = () => {
    fetch("/api/joke")
      .then((res) => res.json())
      .then((data) => {
        joke.value = data.joke;
      });
  };

  const handleReset = () => {
    fetchData();
  };

  const handleCopy = () => {
    navigator.clipboard.writeText(joke.value);
    alert("Copied to clipboard");
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div class="w-full flex flex-col gap-2 items-center">
      <h2>Random joke generator</h2>
      <textarea
        class="w-full bg-gray-100 px-4 py-2 text-gray-900 resize-none rounded"
        rows={5}
        readonly
        value={joke}
      >
      </textarea>
      <div class="flex gap-2">
        <Button onClick={handleReset}>Another</Button>
        <Button onClick={handleCopy}>Copy</Button>
      </div>
    </div>
  );
}
