import Counter from "@components/Counter.tsx";

export default function IndexPage() {
  return (
    <>
      <img
        src="/logo.svg"
        class="w-32 h-32"
        alt="the fresh logo: a sliced lemon dripping with juice"
      />
      <div class="w-48">
        <Counter start={3} />
      </div>
    </>
  );
}
