FROM denoland/deno:latest

WORKDIR /app

COPY . /app

CMD ["deno", "run", "-A", "main.ts"]
