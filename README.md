# fresh project

### Usage

* Start the project:*

```
deno task start
```

This will watch the project directory and restart as necessary.

* Preview the project:*

```
deno task preview
```

This will preview the project just be same as production.
