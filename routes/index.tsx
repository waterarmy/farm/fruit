import { Head } from "$fresh/runtime.ts";
import IndexPage from "@islands/IndexPage.tsx";

export default function IndexRoute() {
  return (
    <>
      <Head>
        <title>Home | Fruit</title>
      </Head>
      <div class="p-4 mx-auto max-w-screen-md flex flex-col justify-center items-center gap-4">
        <IndexPage />
      </div>
    </>
  );
}
