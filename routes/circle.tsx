import { Head } from "$fresh/runtime.ts";
import CirclePage from "@islands/CirclePage.tsx";

export default function IndexRoute() {
  return (
    <>
      <Head>
        <title>Circle | Fruit</title>
      </Head>
      <div class="p-4 mx-auto max-w-screen-md flex flex-col justify-center items-center gap-4">
        <CirclePage />
      </div>
    </>
  );
}
