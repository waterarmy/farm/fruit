import { Head } from "$fresh/runtime.ts";
import JokePage from "@islands/JokePage.tsx";

export default function JokeRoute() {
  return (
    <>
      <Head>
        <title>Joke | Fruit</title>
      </Head>
      <div class="p-4 mx-auto max-w-screen-md flex flex-col justify-center items-center">
        <JokePage />
      </div>
    </>
  );
}
