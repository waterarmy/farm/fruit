import { JSX } from "preact";
import { useSignal } from "@preact/signals";
import Button from "@components/Button.tsx";

interface CounterProps {
  start: number;
}

export default function Counter(props: CounterProps) {
  const { start } = props;

  const count = useSignal(start);

  const handleClick = (e: JSX.TargetedMouseEvent<HTMLButtonElement>) => {
    const type = e.currentTarget.dataset.type;
    console.log(type);
    switch (type) {
      case "increment":
        count.value++;
        break;
      case "decrement":
        count.value--;
        break;
    }
  };

  return (
    <div class="flex gap-2 w-full">
      <Button type="button" data-type="decrement" onClick={handleClick}>
        -
      </Button>
      <p class="flex-grow-1 font-bold text-xl text-center">{count}</p>
      <Button type="button" data-type="increment" onClick={handleClick}>
        +
      </Button>
    </div>
  );
}
