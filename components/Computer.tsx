import { useComputed, useSignal } from "@preact/signals";
import Button from "@components/Button.tsx";

export default function Computer() {
  const radius = useSignal(1);
  const diameter = useComputed(() => {
    console.log("diameter is computing...");
    return radius.value * 2;
  });
  const perimeter = useComputed(() => {
    console.log("perimeter is computing...");
    return diameter.value * Math.PI;
  });
  const area = useComputed(() => {
    console.log("area is computing...");
    return radius.value * radius.value * Math.PI;
  });

  console.log("Computer is rendered once!");

  return (
    <div class="flex flex-col gap-2 items-stretch">
      <h6>
        如果，圆的半径是 <code>{radius}</code> cm
      </h6>
      <p>
        那么，它的直径就是 <code>{diameter}</code> cm
      </p>
      <p>
        那么，它的周长就是 <code>{perimeter}</code> cm
      </p>
      <p>
        那么，它的面积就是 <code>{area}</code> cm<sup>2</sup>
      </p>
      <p class="text-center py-2">
        <Button onClick={() => radius.value++}>click me</Button>
      </p>
    </div>
  );
}
