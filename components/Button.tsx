import { JSX } from "preact";
import { IS_BROWSER } from "$fresh/runtime.ts";

export default function Button(props: JSX.HTMLAttributes<HTMLButtonElement>) {
  console.log(`Button(${props.children}) is rendered once!`);
  return (
    <button
      {...props}
      disabled={!IS_BROWSER || props.disabled}
      class="rounded-sm h-8 px-4 border(gray-100 2) focus:outline-none active:bg-gray-200 active:text-gray-900"
    />
  );
}
